port module Ports exposing (..)


port scrollToElement : String -> Cmd msg


port pushToLog : String -> Cmd msg


port removeFromLog : String -> Cmd msg


port logItem : (String -> msg) -> Sub msg


port deLogItem : (String -> msg) -> Sub msg


port watchedItemNotifiy : String -> Cmd msg


port watchedItemFound : (String -> msg) -> Sub msg


port saveSettings : String -> Cmd msg


port loadConfig : (String -> msg) -> Sub msg
