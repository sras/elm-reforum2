module Common exposing (..)

import Http
import Debug exposing (crash)


convertResultToMessage : (a -> b) -> (String -> b) -> Result Http.Error a -> b
convertResultToMessage msgc err rs =
    case rs of
        Ok a ->
            msgc a

        Err e ->
            err (toString e)


listDiff : List a -> List a -> List a
listDiff loaded requested =
    List.filter
        (\x -> not <| List.member x loaded)
        requested


removeItemFromList : List a -> a -> List a
removeItemFromList l i =
    List.filter (\x -> not <| x == i) l


mapMessage : a -> List b -> (a -> b -> ( b, Cmd a )) -> (a -> c) -> ( List b, Cmd c )
mapMessage msg models update msgConvert =
    let
        updates =
            List.map (update msg) models
    in
        ( List.map Tuple.first updates, Cmd.map msgConvert <| Cmd.batch <| List.map Tuple.second updates )


mapMessageWithField : (d -> a) -> List b -> (b -> d) -> (a -> b -> ( b, Cmd a )) -> (a -> c) -> ( List b, Cmd c )
mapMessageWithField msg models idExtractFn update msgConvert =
    let
        updates =
            List.map (\x -> update (msg <| idExtractFn x) x) models
    in
        ( List.map Tuple.first updates, Cmd.map msgConvert <| Cmd.batch <| List.map Tuple.second updates )


foldModel : List a -> (a -> b -> ( b, Cmd a )) -> b -> ( b, Cmd a )
foldModel msgs update model =
    let
        fn1 : (a -> b -> ( b, Cmd a )) -> a -> ( b, Cmd a ) -> ( b, Cmd a )
        fn1 update msg ( model, cmd ) =
            let
                ( newModel, newCmd ) =
                    update msg model
            in
                ( newModel, Cmd.batch [ cmd, newCmd ] )
    in
        List.foldl (fn1 update) ( model, Cmd.none ) msgs


isJust : Maybe a -> Bool
isJust a =
    case a of
        Just _ ->
            True

        Nothing ->
            False
