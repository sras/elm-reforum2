module Reddit.StoryWidget exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Reddit.Story exposing (..)
import Reddit.Comment exposing (..)
import Reddit.CommentWidget as CW
import Json.Encode
import Common exposing (..)
import Forum exposing (..)
import Ports exposing (..)
import Debug exposing (log, crash)
import Reddit.Api as RD
import Reddit.Common as RDC


type alias Model =
    { story : Story, expanded : Bool, commentWidgetModels : List RDC.CommentWidgetModel, hasMore : Bool, loadedComments : Int }


type Msg
    = Open StoryId
    | Load StoryId
    | AddComments StoryId Url (List Comment)
    | AddComment StoryId Url Comment
    | ErrorMsg StoryId String
    | CommentMsg StoryId RDC.CommentWidgetMsg
    | Collapse StoryId
    | CollapseComments StoryId
    | LoadMoreComments StoryId


view : Model -> Html Msg
view model =
    let
        loadMoreComments =
            if model.story.childCount > 0 then
                if not model.expanded then
                    span [] <|
                        [ span [ class "link", onClick (Open model.story.id) ] [ text "Open" ]
                        ]
                else
                    span [] []
            else
                span [] []

        items =
            [ i [ class "author" ] [ text "- ", text <| model.story.poster ]
            , small [] [ text <| (toString model.story.childCount) ++ " comments" ]
            , loadMoreComments
            , case model.story.url of
                Just l ->
                    a [ target "_blank", class "link", href l ] [ text "Link" ]

                Nothing ->
                    span [] []
            , a [ class "link", target "_blank", href model.story.linkToDiscussion ] [ text "Forum link" ]
            , if model.expanded then
                span []
                    [ span [ class "link", onClick <| Collapse model.story.id ] [ text "Collapse" ]
                    , span [ class "link", onClick <| Load model.story.id ] [ text "Refresh" ]
                    , if model.hasMore then
                        span [ class "link", onClick <| LoadMoreComments model.story.id ] [ text "Load more comments" ]
                      else
                        span [] []
                    , span
                        [ class "link", onClick <| CollapseComments model.story.id ]
                        [ text <| "Top Level Comments Only(" ++ (toString model.loadedComments) ++ ")" ]
                    ]
              else
                span []
                    [ span [ class "link", onClick <| Load model.story.id ] [ text "Refresh" ]
                    ]
            ]

        withComments =
            if model.expanded then
                items ++ [ div [] <| List.map (\x -> Html.map (CommentMsg model.story.id) <| CW.view x) model.commentWidgetModels ]
            else
                items
    in
        div
            [ id <| RDC.makeStoryElementId model.story
            , if not model.expanded then
                class "container-fluid story-container"
              else
                class "container-fluid story-container expanded"
            ]
            [ div [ class "row" ]
                [ div [ class "r-border sr-border", onClick <| Collapse model.story.id, property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                , div [ class "col-11" ]
                    [ div [ class "story-title" ] [ text <| model.story.title ]
                    , if model.expanded then
                        case model.story.text of
                            Just x ->
                                div [ class "selftext-container", property "innerHTML" (Json.Encode.string x) ] []

                            Nothing ->
                                span [] []
                      else
                        span [] []
                    , div [] withComments
                    ]
                ]
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    if getIdx msg == model.story.id then
        case msg of
            ErrorMsg _ msg ->
                log msg <| ( model, Cmd.none )

            CollapseComments _ ->
                let
                    ( models, cmd ) =
                        mapMessageWithField (\id -> RDC.Collapse id False) model.commentWidgetModels RDC.getCommentIdFromModel CW.update (CommentMsg <| model.story.id)
                in
                    ( { model | commentWidgetModels = models }, cmd )

            LoadMoreComments _ ->
                let
                    existingCommentIds =
                        List.filterMap getIdIfComment model.commentWidgetModels

                    moreCommentIds =
                        RDC.findMoreCommentItemIds model.commentWidgetModels

                    newCommentIds =
                        List.filter (\x -> not <| List.member x existingCommentIds) moreCommentIds

                    cmds =
                        Cmd.batch <| List.map (RD.getComment (\x -> convertResultToMessage (AddComment model.story.id x) (ErrorMsg model.story.id)) <| String.dropRight 1 model.story.linkToDiscussion) newCommentIds

                    filteredModels =
                        List.filter (not << RDC.isMore) model.commentWidgetModels
                in
                    ( { model | commentWidgetModels = filteredModels, hasMore = RDC.hasMoreItems <| List.map (\(RDC.CommentWidgetModel c) -> c.comment) filteredModels }, cmds )

            CommentMsg _ cmsg ->
                let
                    ( models, cmd ) =
                        mapMessage cmsg model.commentWidgetModels CW.update (CommentMsg model.story.id)
                in
                    ( { model | commentWidgetModels = models }, cmd )

            AddComment _ url comment ->
                let
                    existingCommentIds =
                        List.filterMap getIdIfComment model.commentWidgetModels
                in
                    if (List.member (getId comment) existingCommentIds) then
                        ( model, removeFromLog url )
                    else
                        let
                            newModel =
                                CW.makeModel comment

                            models =
                                newModel :: model.commentWidgetModels
                        in
                            ( { model | loadedComments = List.length models, hasMore = RDC.isMore newModel, commentWidgetModels = models }, removeFromLog url )

            AddComments _ url comments ->
                let
                    existingCommentIds =
                        List.map (\(RDC.CommentWidgetModel m) -> getId m.comment) model.commentWidgetModels

                    freshComments =
                        List.filter (\x -> not <| List.member (getId x) existingCommentIds) comments

                    newModels =
                        (List.map CW.makeModel freshComments)

                    models =
                        newModels ++ model.commentWidgetModels
                in
                    ( { model | hasMore = RDC.hasMoreItems freshComments, commentWidgetModels = models, loadedComments = List.length models }, removeFromLog url )

            Open _ ->
                let
                    ( newModel, cmd ) =
                        if List.length model.commentWidgetModels == 0 then
                            update (Load model.story.id) model
                        else
                            ( model, Cmd.none )
                in
                    ( { newModel | expanded = True }, cmd )

            Load _ ->
                let
                    cmd =
                        RD.getComments model.story (\x -> convertResultToMessage (AddComments model.story.id x) (ErrorMsg model.story.id))
                in
                    ( model, cmd )

            Collapse _ ->
                ( { model | expanded = False }
                , let
                    (StoryId id) =
                        model.story.id
                  in
                    scrollToElement <| RDC.makeStoryElementId model.story
                )
    else
        ( model, Cmd.none )


makeModel : Story -> Model
makeModel story =
    { story = story, expanded = False, commentWidgetModels = [], hasMore = False, loadedComments = 0 }


getIdx : Msg -> StoryId
getIdx msg =
    case msg of
        Open idx ->
            idx

        Load idx ->
            idx

        ErrorMsg idx _ ->
            idx

        AddComments idx _ _ ->
            idx

        AddComment idx _ _ ->
            idx

        CommentMsg idx _ ->
            idx

        Collapse idx ->
            idx

        LoadMoreComments idx ->
            idx

        CollapseComments idx ->
            idx


getIdIfComment : RDC.CommentWidgetModel -> Maybe CommentId
getIdIfComment (RDC.CommentWidgetModel c) =
    case c.comment of
        Comment x ->
            Just x.id

        Deleted x ->
            Just x

        _ ->
            Nothing
