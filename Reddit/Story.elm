module Reddit.Story exposing (..)

import Forum exposing (..)


type StoryId
    = StoryId String


type alias Story =
    { subreddit : Subreddit, title : String, url : Maybe Url, linkToDiscussion : String, poster : String, text : Maybe String, id : StoryId, childCount : Int }
