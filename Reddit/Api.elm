module Reddit.Api exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Http
import Json.Decode as Decode
import Forum exposing (..)
import Reddit.Story exposing (..)
import Reddit.Comment exposing (..)
import Reddit.Common as RDC
import Ports exposing (..)


commentsDecoder : Decode.Decoder (List Comment)
commentsDecoder =
    Decode.index 1 prefixDecoder


commentDecoder : Decode.Decoder Comment
commentDecoder =
    Decode.map7 makeComment (Decode.field "id" Decode.string) (Decode.field "body_html" Decode.string) (Decode.map getRedditUrl <| Decode.field "permalink" Decode.string) (Decode.field "author" Decode.string) (Decode.field "replies" (Decode.lazy (\_ -> Decode.oneOf [ prefixDecoder, emptyStringDecoder ]))) (Decode.field "score" Decode.int) (Decode.field "controversiality" Decode.int)


moreCommentItemDecoder : Decode.Decoder Comment
moreCommentItemDecoder =
    let
        makeMoreItem : String -> Int -> List CommentId -> Comment
        makeMoreItem id count commentIds =
            More { id = CommentId id, count = count, commentIds = commentIds }
    in
        Decode.map3 makeMoreItem (Decode.field "id" Decode.string) (Decode.field "count" Decode.int) (Decode.field "children" <| Decode.list <| Decode.map CommentId Decode.string)


emptyStringDecoder : Decode.Decoder (List Comment)
emptyStringDecoder =
    let
        fn : String -> List Comment
        fn s =
            if s == "" then
                []
            else
                crash "Bad comment list"
    in
        Decode.map fn Decode.string


singleCommentDecoder : Decode.Decoder Comment
singleCommentDecoder =
    Decode.index 1 <|
        Decode.field "data" <|
            Decode.field "children" <|
                Decode.index 0 <|
                    Decode.field "data" <|
                        commentDecoder


deletedSingleCommentDecoder : CommentId -> Decode.Decoder Comment
deletedSingleCommentDecoder commentId =
    let
        emptyListToComment : List String -> Comment
        emptyListToComment l =
            if List.length l == 0 then
                Deleted commentId
            else
                crash "Unexpected"
    in
        Decode.index 1 <|
            Decode.field "data" <|
                Decode.field "children" <|
                    Decode.map emptyListToComment <|
                        Decode.list <|
                            Decode.string


prefixDecoder : Decode.Decoder (List Comment)
prefixDecoder =
    Decode.field "data" <|
        Decode.field "children" <|
            Decode.list <|
                Decode.field "data" <|
                    Decode.oneOf [ commentDecoder, moreCommentItemDecoder ]


storyDecoder : Subreddit -> Decode.Decoder Story
storyDecoder subreddit =
    Decode.index 0 <|
        Decode.field "data" <|
            Decode.field "children" <|
                Decode.index 0 <|
                    Decode.field "data" <|
                        storySegDecoder subreddit


makeStory subreddit a1 a2 a3 a4 a5 a6 a7 =
    Story subreddit a1 a2 a3 a4 a5 a6 a7


makeComment id body permalink author replies score controversiality =
    Comment { id = CommentId id, subreddit = "", text = body, linkToComment = permalink, author = author, replies = replies, score = score, controversiality = controversiality }


storySegDecoder : Subreddit -> Decode.Decoder Story
storySegDecoder subreddit =
    Decode.map7 (makeStory subreddit) (Decode.field "title" Decode.string) (Decode.maybe <| Decode.field "url" Decode.string) (Decode.map getRedditUrl <| Decode.field "permalink" Decode.string) (Decode.field "author" Decode.string) (Decode.maybe <| Decode.field "selftext_html" Decode.string) (Decode.map StoryId <| Decode.field "id" Decode.string) (Decode.field "num_comments" Decode.int)


sublistingDecoder : Subreddit -> Decode.Decoder (List Story)
sublistingDecoder subreddit =
    Decode.field "data" <| Decode.field "children" <| Decode.list (Decode.field "data" <| storySegDecoder subreddit)


getComment : (String -> Result Http.Error Comment -> b) -> Url -> CommentId -> Cmd b
getComment msfn storyUrl commentId =
    let
        (CommentId id) =
            commentId

        url =
            getJsonUrl <| storyUrl ++ "/" ++ id
    in
        Cmd.batch [ pushToLog url, Http.send (msfn url) <| Http.get url <| Decode.oneOf [ singleCommentDecoder, deletedSingleCommentDecoder commentId ] ]


getComments : Story -> (String -> Result Http.Error (List Comment) -> b) -> Cmd b
getComments story msfn =
    let
        url =
            (story.linkToDiscussion ++ "/.json?raw_json=1")
    in
        Cmd.batch [ pushToLog url, Http.send (msfn url) <| Http.get url <| commentsDecoder ]


getRedditUrl : String -> String
getRedditUrl permalink =
    "https://www.reddit.com" ++ permalink



-- ^^ That www. prefix is important here. Without it the response won't have the life saving CORS allow-origin header.


getRedditJsonUrl : String -> String
getRedditJsonUrl permalink =
    getJsonUrl <| (getRedditUrl permalink)


getJsonUrl : String -> String
getJsonUrl u =
    u ++ "/.json?raw_json=1"


getStories : Subreddit -> (String -> Result Http.Error (List Story) -> b) -> Cmd b
getStories subname msfn =
    let
        url =
            (getRedditJsonUrl ("/r/" ++ subname))
    in
        Cmd.batch [ pushToLog url, Http.send (msfn url) <| Http.get url <| (sublistingDecoder <| subname) ]
