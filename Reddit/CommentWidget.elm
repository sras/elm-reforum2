module Reddit.CommentWidget exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Reddit.Comment exposing (..)
import Json.Encode
import Common exposing (..)
import Forum exposing (..)
import Ports exposing (..)
import Debug exposing (log, crash)
import Reddit.Api as RD
import Reddit.Common exposing (..)


update : CommentWidgetMsg -> CommentWidgetModel -> ( CommentWidgetModel, Cmd CommentWidgetMsg )
update msg (CommentWidgetModel model) =
    if getIdx msg == getId model.comment then
        case msg of
            ChildCommentMsg _ msg ->
                case msg of
                    MessageForParent cid pms ->
                        case pms of
                            SendCommentText ->
                                let
                                    ( models, cmd ) =
                                        mapMessage (LoadParentText cid <| getText model.comment) model.commentWidgetModels update <| ChildCommentMsg <| getId model.comment
                                in
                                    ( CommentWidgetModel { model | commentWidgetModels = models }, cmd )

                    _ ->
                        let
                            ( models, cmd ) =
                                mapMessage msg model.commentWidgetModels update <| ChildCommentMsg <| getId model.comment
                        in
                            ( CommentWidgetModel { model | commentWidgetModels = models }, cmd )

            Open _ ->
                ( CommentWidgetModel { model | expanded = True }, Cmd.none )

            Collapse _ scrollIntoView ->
                ( CommentWidgetModel { model | expanded = False }
                , if scrollIntoView then
                    scrollToElement <| makeCommentElementId model.comment
                  else
                    Cmd.none
                )

            CollapseReplies _ ->
                let
                    ( models, cmd ) =
                        mapMessageWithField (\id -> Collapse id False) model.commentWidgetModels getCommentIdFromModel update (ChildCommentMsg <| getId model.comment)
                in
                    ( CommentWidgetModel { model | commentWidgetModels = models }, cmd )

            LoadParentText _ text ->
                ( CommentWidgetModel { model | parentText = Just text }, Cmd.none )

            AddReply _ url comment ->
                let
                    models =
                        makeModel comment :: model.commentWidgetModels
                in
                    ( CommentWidgetModel { model | commentWidgetModels = models, loadedReplies = List.length models }, removeFromLog url )

            LoadMoreReplies _ ->
                let
                    getIdIfComment : CommentWidgetModel -> Maybe CommentId
                    getIdIfComment (CommentWidgetModel c) =
                        case c.comment of
                            Comment x ->
                                Just x.id

                            Deleted x ->
                                Just x

                            _ ->
                                Nothing

                    existingCommentIds =
                        List.filterMap getIdIfComment model.commentWidgetModels

                    moreCommentIds =
                        findMoreCommentItemIds model.commentWidgetModels

                    newCommentIds =
                        List.filter (\x -> not <| List.member x existingCommentIds) moreCommentIds

                    storyUrl =
                        String.dropRight 1 <| String.join "/" <| List.reverse <| List.drop 2 <| List.reverse <| String.split "/" <| getLinkToComment model.comment

                    cmds =
                        Cmd.batch <| List.map (RD.getComment (\x -> convertResultToMessage (AddReply (getId model.comment) x) (ErrorMsg <| getId model.comment)) <| storyUrl) newCommentIds
                in
                    ( CommentWidgetModel { model | hasMore = False, commentWidgetModels = List.filter (not << isMore) model.commentWidgetModels }
                    , cmds
                    )

            MessageForParent _ msg ->
                ( CommentWidgetModel model, Cmd.none )

            ErrorMsg _ error ->
                ( CommentWidgetModel model, Cmd.none )
    else
        ( CommentWidgetModel model, Cmd.none )


view : CommentWidgetModel -> Html CommentWidgetMsg
view (CommentWidgetModel model) =
    case model.comment of
        Deleted _ ->
            div [ class "comment-container" ] [ text "-- Deleted Comment --" ]

        More _ ->
            div [] []

        Comment comment ->
            let
                loadMoreComments =
                    if not model.expanded then
                        span []
                            [ if model.loadedReplies > 0 then
                                span [ class "link", onClick (Open (getId model.comment)) ] [ text ("Open " ++ (toString model.loadedReplies) ++ " replies") ]
                              else
                                span [] []
                            , span [] [ span [ class "link" ] [ text "Refresh" ] ]
                            , a [ class "link", target "_blank", href comment.linkToComment ] [ text "Forum link" ]
                            , if model.hasMore then
                                span [ class "link", onClick (LoadMoreReplies <| getId model.comment) ] [ text "Load more replies" ]
                              else
                                span [] []
                            ]
                    else
                        span []
                            [ if model.loadedReplies > 0 then
                                span [ class "link", onClick <| Collapse (getId model.comment) True ]
                                    [ text <| "Collapse " ++ (toString model.loadedReplies) ++ " comments" ]
                              else
                                span [] []
                            , span [ class "link" ] [ text "Refresh" ]
                            , span [ class "link", onClick <| MessageForParent (getId model.comment) SendCommentText ] [ text "Show parent" ]
                            , a [ class "link", target "_blank", href comment.linkToComment ] [ text "Forum link" ]
                            , if model.hasMore then
                                span [ class "link", onClick (LoadMoreReplies <| getId model.comment) ] [ text "Load more replies" ]
                              else
                                span [] []
                            , span
                                [ class "link", onClick <| CollapseReplies <| getId model.comment ]
                                [ text "Top Level Comments Only" ]
                            ]
            in
                div
                    [ if not model.expanded then
                        class "container-fluid comment-container"
                      else
                        class "container-fluid comment-container expanded"
                    , id <| makeCommentElementId model.comment
                    ]
                    [ div [ class "row" ]
                        [ div [ class "r-border cr-border", onClick <| Collapse (getId model.comment) True, property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                        , div [ class "col", style [ ( "padding-right", "0px" ) ] ]
                            [ case model.parentText of
                                Just pt ->
                                    div [ class "parent-comment", property "innerHTML" <| Json.Encode.string <| pt ] []

                                Nothing ->
                                    span [] []
                            , div [ property "innerHTML" <| Json.Encode.string <| comment.text ] []
                            , span []
                                [ i [ class "author" ] [ text "- ", text comment.author ]
                                ]
                            , span [] [ loadMoreComments ]
                            , if model.expanded then
                                div [] <| List.map (\x -> Html.map (ChildCommentMsg comment.id) <| view x) model.commentWidgetModels
                              else
                                span [] []
                            ]
                        ]
                    ]


makeModel : Comment -> CommentWidgetModel
makeModel co =
    let
        replies =
            getReplies co

        models =
            List.map makeModel replies
    in
        CommentWidgetModel { comment = co, expanded = True, commentWidgetModels = models, hasMore = hasMoreItems replies, loadedReplies = List.length models, parentText = Nothing }


getIdx : CommentWidgetMsg -> CommentId
getIdx msg =
    case msg of
        Open idx ->
            idx

        ChildCommentMsg idx _ ->
            idx

        Collapse idx _ ->
            idx

        CollapseReplies idx ->
            idx

        LoadMoreReplies idx ->
            idx

        AddReply idx _ _ ->
            idx

        ErrorMsg idx _ ->
            idx

        MessageForParent idx _ ->
            idx

        LoadParentText idx _ ->
            idx
