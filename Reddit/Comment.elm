module Reddit.Comment exposing (..)

import Forum exposing (..)
import Debug exposing (crash)


type CommentId
    = CommentId String


type Comment
    = Comment { id : CommentId, subreddit : Subreddit, text : String, linkToComment : String, author : String, replies : List Comment, score : Int, controversiality : Int }
    | Deleted CommentId
    | More { id : CommentId, count : Int, commentIds : List CommentId }


getId : Comment -> CommentId
getId c =
    case c of
        Comment c ->
            c.id

        Deleted id ->
            id

        More m ->
            m.id


getForum : Comment -> Subreddit
getForum c =
    case c of
        Comment m ->
            m.subreddit

        Deleted _ ->
            crash "Forum not available for deleted comment"

        More m ->
            crash "Forum not available for more entry"


getText : Comment -> String
getText comment =
    case comment of
        Comment c ->
            c.text

        Deleted _ ->
            "--deleted comment--"

        More _ ->
            ""


getReplies : Comment -> List Comment
getReplies co =
    case co of
        Comment c ->
            c.replies

        Deleted _ ->
            []

        More _ ->
            []


getLinkToComment : Comment -> String
getLinkToComment co =
    case co of
        Comment c ->
            c.linkToComment

        Deleted _ ->
            crash ""

        More _ ->
            crash ""
