module Reddit.Common exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Reddit.Comment exposing (..)
import Reddit.Story exposing (..)
import Json.Encode
import Forum exposing (..)
import Debug exposing (log)


type ParentMessage
    = SendCommentText


type CommentWidgetMsg
    = ChildCommentMsg CommentId CommentWidgetMsg
    | Open CommentId
    | Collapse CommentId Bool
    | CollapseReplies CommentId
    | AddReply CommentId Url Comment
    | LoadMoreReplies CommentId
    | ErrorMsg CommentId String
    | MessageForParent CommentId ParentMessage
    | LoadParentText CommentId String


type CommentWidgetModel
    = CommentWidgetModel { comment : Comment, expanded : Bool, commentWidgetModels : List CommentWidgetModel, hasMore : Bool, loadedReplies : Int, parentText : Maybe String }


isMore : CommentWidgetModel -> Bool
isMore (CommentWidgetModel c) =
    case c.comment of
        More _ ->
            True

        _ ->
            False


hasMoreItems : List Comment -> Bool
hasMoreItems comments =
    let
        isMore : Comment -> Bool
        isMore c =
            case c of
                More x ->
                    True

                _ ->
                    False
    in
        (List.length <| List.filter isMore comments) > 0


findMoreCommentItemIds : List CommentWidgetModel -> List CommentId
findMoreCommentItemIds commentWidgetModels =
    let
        extractIds : CommentWidgetModel -> List CommentId
        extractIds (CommentWidgetModel m) =
            case m.comment of
                More x ->
                    x.commentIds

                _ ->
                    []
    in
        List.concatMap extractIds commentWidgetModels


getCommentIdFromModel : CommentWidgetModel -> CommentId
getCommentIdFromModel (CommentWidgetModel model) =
    getId <| model.comment


makeStoryElementId : Story -> String
makeStoryElementId s =
    let
        (StoryId id) =
            s.id
    in
        "story-container-" ++ id


makeCommentElementId : Comment -> String
makeCommentElementId c =
    let
        (CommentId id) =
            getId c
    in
        "comment-container-" ++ id
