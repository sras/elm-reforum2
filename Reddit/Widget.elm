module Reddit.Widget exposing (..)

import Debug exposing (log, crash)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Debug exposing (log, crash)
import Reddit.StoryWidget as SW
import Reddit.Story exposing (..)
import Reddit.Api as RD
import Common exposing (..)
import Forum exposing (..)
import Ports exposing (..)
import Json.Encode


type alias Model =
    { idx : Int, subreddit : Subreddit, storyWidgetModels : List SW.Model, expanded : Bool }


type Msg
    = StoryMsg Int SW.Msg
    | Load Int
    | AddStories Int Url (List Story)
    | NetworkErrorMsg Int Url String
    | Expand Int
    | Collapse Int
    | Remove Int


view : Model -> Html Msg
view model =
    div [ class "forum-container", title "Tracking protection should be disabled for reddit api to work." ]
        [ div
            [ class "container-fluid" ]
            [ div [ class "row" ]
                [ div [ class "fr-border r-border", onClick <| Collapse model.idx, property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                , div [ class "col-11" ]
                    [ span
                        [ onClick <|
                            if model.expanded then
                                Collapse model.idx
                            else
                                Expand model.idx
                        , class "forum-heading"
                        ]
                        [ text <| "/r/" ++ model.subreddit ]
                    , if model.expanded then
                        span [] []
                      else
                        span [ onClick <| Expand model.idx, class "link" ] [ text "open" ]
                    , span [ onClick <| Load model.idx, class "link" ] [ text "reload" ]
                    , span [ onClick <| Remove model.idx, class "link" ] [ text "remove" ]
                    , if model.expanded then
                        div [] <| List.map (\x -> Html.map (StoryMsg model.idx) <| SW.view x) model.storyWidgetModels
                      else
                        span [] []
                    ]
                ]
            ]
        ]


update : Settings -> Msg -> Model -> ( Model, Cmd Msg )
update settings msg model =
    if getIdx msg == model.idx then
        case msg of
            Expand _ ->
                if List.length model.storyWidgetModels == 0 then
                    update settings (Load model.idx) { model | expanded = True }
                else
                    ( { model | expanded = True }, Cmd.none )

            Collapse _ ->
                ( { model | expanded = False }, Cmd.none )

            Load _ ->
                let
                    cmd =
                        RD.getStories model.subreddit (\x -> convertResultToMessage (AddStories model.idx x) (NetworkErrorMsg model.idx x))
                in
                    ( model, cmd )

            AddStories _ url rawStories ->
                let
                    existingIds =
                        List.map (\x -> x.story.id) model.storyWidgetModels

                    newStories =
                        List.filter (\x -> not <| List.member x.id existingIds) rawStories

                    stories =
                        List.filter (shouldAllow settings.filters) newStories
                in
                    ( { model | storyWidgetModels = (List.map (\story -> SW.makeModel story) stories) ++ model.storyWidgetModels }, Cmd.batch [ Cmd.batch <| List.map (processWatches model.subreddit settings.watches) stories, removeFromLog url ] )

            StoryMsg _ smg ->
                let
                    ( models, cmd ) =
                        mapMessage smg model.storyWidgetModels SW.update (StoryMsg model.idx)
                in
                    ( { model | storyWidgetModels = models }, cmd )

            NetworkErrorMsg _ url msg ->
                log msg ( model, removeFromLog url )

            Remove idx ->
                ( model, Cmd.none )
    else
        ( model, Cmd.none )


processWatches : Subreddit -> List Watch -> Story -> Cmd Msg
processWatches sub watches story =
    let
        checkFn : Subreddit -> Watch -> Bool
        checkFn sub w =
            w.forum == Reddit sub && (String.contains w.term <| String.toLower story.title)
    in
        if (List.length <| List.filter (checkFn sub) watches) > 0 then
            watchedItemNotifiy story.title
        else
            Cmd.none


shouldAllow : List String -> Story -> Bool
shouldAllow filters story =
    (List.length <| List.filter (\x -> String.contains x <| String.toLower story.title) filters) == 0


init : Subreddit -> Int -> ( Model, Cmd Msg )
init sub idx =
    ( { idx = idx, subreddit = sub, storyWidgetModels = [], expanded = False }, Cmd.none )


getIdx : Msg -> Int
getIdx msg =
    case msg of
        StoryMsg idx _ ->
            idx

        AddStories idx _ _ ->
            idx

        Load idx ->
            idx

        NetworkErrorMsg idx _ _ ->
            idx

        Expand idx ->
            idx

        Collapse idx ->
            idx

        Remove idx ->
            idx
