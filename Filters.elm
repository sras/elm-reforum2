module Filters exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Forum exposing (..)
import HackerNews.Widget as HN
import Reddit.Widget as RD
import Common exposing (..)
import Ports exposing (..)
import Http


type Msg
    = Add
    | AddFilter String
    | Remove String
    | FilterStringInput String


type alias Model =
    { filters : List String, newFilter : String }


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ div []
            [ input [ onInput FilterStringInput, value model.newFilter ] []
            , button [ onClick Add ] [ text "Add" ]
            ]
        , if List.length model.filters > 0 then
            div []
                [ h3 []
                    [ text "Filters..."
                    ]
                ]
          else
            div [] [ text "No Filters yet.." ]
        , div [] <| List.map (\x -> div [] [ text x, span [ onClick <| Remove x, class "link" ] [ text "remove" ] ]) model.filters
        ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        Add ->
            { model | filters = (String.toLower model.newFilter) :: model.filters, newFilter = "" }

        AddFilter s ->
            { model | filters = (String.toLower s) :: model.filters }

        FilterStringInput s ->
            { model | newFilter = s }

        Remove s ->
            { model | filters = List.filter (\x -> not <| x == s) model.filters }


init : Model
init =
    { filters = [], newFilter = "" }
