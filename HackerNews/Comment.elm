module HackerNews.Comment exposing (..)

import Forum exposing (..)
import Debug exposing (crash)


type CommentId
    = CommentId String


type Comment
    = Comment { id : CommentId, section : Section, text : String, linkToComment : String, author : String, childIds : Maybe (List CommentId) }
    | Deleted CommentId


getId : Comment -> CommentId
getId c =
    case c of
        Comment c ->
            c.id

        Deleted id ->
            id


getText : Comment -> String
getText c =
    case c of
        Comment c ->
            c.text

        Deleted id ->
            ""


getForum : Comment -> Section
getForum c =
    case c of
        Comment m ->
            m.section

        Deleted _ ->
            crash "Forum not available for deleted comment"
