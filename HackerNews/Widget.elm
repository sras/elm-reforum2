module HackerNews.Widget exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log)
import String exposing (toInt)
import Random exposing (..)
import Forum exposing (..)
import HackerNews.Api as HN
import HackerNews.Story exposing (..)
import HackerNews.StoryWidget as SW
import Common exposing (..)
import Ports exposing (..)
import Json.Encode
import Http


type alias Model =
    { section : Section
    , storyWidgetModels : List SW.Model
    , idx : Int
    , expanded : Bool
    }


type Msg
    = LoadStories Int String (List StoryId)
    | AddStory Int String Story
    | StoryMsg Int SW.Msg
    | NetworkErrorMsg Int String String
    | Load Int
    | Expand Int
    | Collapse Int
    | Remove Int


view : Model -> Html Msg
view model =
    div [ class "forum-container" ]
        [ div
            [ class "container-fluid" ]
            [ div [ class "row" ]
                [ div [ class "fr-border r-border", onClick <| Collapse model.idx, property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                , div [ class "col-11" ]
                    [ span
                        [ onClick <|
                            (if model.expanded then
                                Collapse model.idx
                             else
                                Expand model.idx
                            )
                        , class "forum-heading"
                        ]
                        [ text <| "HN : " ++ model.section
                        ]
                    , if model.expanded then
                        span [] []
                      else
                        span [ onClick <| Expand model.idx, class "link" ] [ text "open" ]
                    , span [ onClick <| Load model.idx, class "link" ] [ text "reload" ]
                    , span [ onClick <| Remove model.idx, class "link" ] [ text "remove" ]
                    , if model.expanded then
                        div [] [ div [] (List.map (\x -> Html.map (StoryMsg model.idx) <| SW.view x) model.storyWidgetModels) ]
                      else
                        span [] []
                    ]
                ]
            ]
        ]


init : Section -> Int -> ( Model, Cmd Msg )
init section idx =
    ( { section = section, storyWidgetModels = [], idx = idx, expanded = False }, Cmd.none )


update : Settings -> Msg -> Model -> ( Model, Cmd Msg )
update settings msg model =
    if getIdx msg == model.idx then
        case msg of
            Expand _ ->
                if List.length model.storyWidgetModels == 0 then
                    update settings (Load model.idx) { model | expanded = True }
                else
                    ( { model | expanded = True }, Cmd.none )

            Collapse _ ->
                ( { model | expanded = False }, Cmd.none )

            LoadStories idx url ids ->
                let
                    msgfn x =
                        convertResultToMessage (AddStory idx x) (NetworkErrorMsg idx x)

                    existingIds =
                        List.map (\x -> x.story.id) model.storyWidgetModels

                    newIds =
                        List.filter (\x -> not <| List.member x existingIds) ids

                    cmds : List (Cmd Msg)
                    cmds =
                        List.map (HN.getStory model.section msgfn) <| newIds
                in
                    ( model, Cmd.batch (removeFromLog url :: cmds) )

            AddStory idx url msg ->
                let
                    insertStory : Story -> List SW.Model -> List SW.Model
                    insertStory story models =
                        let
                            storyModel =
                                SW.makeModel story
                        in
                            case models of
                                [] ->
                                    [ storyModel ]

                                h :: t ->
                                    if story.score > h.story.score then
                                        storyModel :: models
                                    else
                                        h :: (insertStory story t)
                in
                    if shouldAllow settings.filters msg then
                        ( { model | storyWidgetModels = insertStory msg model.storyWidgetModels }, Cmd.batch [ processWatches model.section settings.watches msg, removeFromLog url ] )
                    else
                        ( model, removeFromLog url )

            StoryMsg idx msg ->
                let
                    ( models, cmd ) =
                        mapMessage msg model.storyWidgetModels SW.update <| StoryMsg idx
                in
                    ( { model | storyWidgetModels = models }, cmd )

            Remove idx ->
                ( model, Cmd.none )

            Load idx ->
                let
                    cmd =
                        HN.getStories model.section (\x -> convertResultToMessage (LoadStories idx x) (NetworkErrorMsg idx x))
                in
                    ( model, cmd )

            NetworkErrorMsg idx url msg ->
                log (toString msg) <| ( model, removeFromLog url )
    else
        ( model, Cmd.none )


processWatches : Section -> List Watch -> Story -> Cmd Msg
processWatches section watches story =
    let
        checkFn : Watch -> Bool
        checkFn w =
            w.forum == HackerNews section && (String.contains w.term <| String.toLower story.title)
    in
        if (List.length <| List.filter checkFn watches) > 0 then
            watchedItemNotifiy story.title
        else
            Cmd.none


shouldAllow : List String -> Story -> Bool
shouldAllow filters story =
    (List.length <| List.filter (\x -> String.contains x <| String.toLower story.title) filters) == 0


getIdx : Msg -> Int
getIdx msg =
    case msg of
        LoadStories idx _ _ ->
            idx

        AddStory idx _ _ ->
            idx

        StoryMsg idx _ ->
            idx

        NetworkErrorMsg idx _ _ ->
            idx

        Load idx ->
            idx

        Expand idx ->
            idx

        Collapse idx ->
            idx

        Remove idx ->
            idx
