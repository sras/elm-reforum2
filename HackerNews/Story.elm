module HackerNews.Story exposing (..)

import HackerNews.Comment exposing (..)
import Forum exposing (..)


type StoryId
    = StoryId String


type alias Story =
    { forum : Section, title : String, url : Maybe String, linkToDiscussion : String, poster : String, text : Maybe String, id : StoryId, childCount : Int, childIds : Maybe (List CommentId), comments : List Comment, score : Int }
