module HackerNews.StoryWidget exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Forum exposing (..)
import HackerNews.Api as HN
import HackerNews.Story exposing (..)
import HackerNews.Comment exposing (..)
import HackerNews.CommentWidget as CW
import Common exposing (..)
import Json.Encode
import Http
import Ports exposing (..)


type alias Model =
    { story : Story
    , expanded : Bool
    , commentWidgetModels : List CW.Model
    }


type Msg
    = LoadComments StoryId Story (String -> Comment -> Msg)
    | ReplaceStory StoryId Bool String Story
    | AddComment StoryId String Comment
    | AddCommentAndLoadReplies StoryId String Comment
    | HideComments StoryId
    | HideCommentsRec StoryId
    | CommentMsg StoryId CW.Msg
    | ErrorMsg StoryId String
    | Refresh StoryId


view : Model -> Html Msg
view model =
    let
        loadMoreComments =
            if model.story.childCount > 0 then
                if not model.expanded then
                    span [] <|
                        [ span [ class "link", onClick (LoadComments model.story.id model.story <| AddComment model.story.id) ] [ text ("Load comments(" ++ (toString model.story.childCount) ++ ")") ]
                        , span [ class "link", onClick (LoadComments model.story.id model.story <| AddCommentAndLoadReplies model.story.id) ] [ text "Load all comments" ]
                        ]
                else
                    span [] []
            else
                span [] []

        items =
            [ i [ class "author" ] [ text "- ", text <| model.story.poster ]
            , loadMoreComments
            , case model.story.url of
                Just l ->
                    a [ class "link", target "_blank", href l ] [ text "Link" ]

                Nothing ->
                    span [] []
            , a [ class "link", target "_blank", href model.story.linkToDiscussion ] [ text "Forum link" ]
            , if model.expanded then
                span []
                    [ span [ class "link", onClick (HideComments model.story.id) ] [ text "Collapse" ]
                    , span [ class "link", onClick (Refresh model.story.id) ] [ text "Refresh" ]
                    , span [ class "link", onClick (LoadComments model.story.id model.story <| AddCommentAndLoadReplies model.story.id) ] [ text "Load all comments" ]
                    , span
                        [ class "link", onClick (HideCommentsRec model.story.id) ]
                        [ text "Top Level Comments Only" ]
                    ]
              else
                span []
                    [ span [ class "link", onClick (Refresh model.story.id) ] [ text "Refresh" ]
                    ]
            ]

        withComments =
            if model.expanded then
                items ++ [ div [] <| List.map (\x -> Html.map (CommentMsg model.story.id) <| CW.view x) model.commentWidgetModels ]
            else
                items
    in
        div
            [ id <| "story-" ++ (toString model.story.id)
            , if not model.expanded then
                class "container-fluid story-container"
              else
                class "container-fluid story-container expanded"
            ]
            [ div [ class "row" ]
                [ div [ class "r-border sr-border", onClick (HideComments model.story.id), property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                , div [ class "col-11" ]
                    [ div [ class "story-title" ] [ text <| model.story.title ]
                    , if model.expanded then
                        case model.story.text of
                            Just x ->
                                div [ class "selftext-container", property "innerHTML" (Json.Encode.string x) ] []

                            Nothing ->
                                span [] []
                      else
                        span [] []
                    , div [] withComments
                    ]
                ]
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    if getIdx msg == model.story.id then
        case msg of
            ErrorMsg _ err ->
                log (toString err) ( model, Cmd.none )

            CommentMsg _ cmsg ->
                let
                    ( models, cmd ) =
                        mapMessage cmsg model.commentWidgetModels CW.update (CommentMsg model.story.id)
                in
                    ( { model | commentWidgetModels = models }, cmd )

            HideComments _ ->
                ( { model | expanded = False }, scrollToElement ("story-" ++ (toString model.story.id)) )

            Refresh _ ->
                ( model, HN.getStory (model.story.forum) (\x -> convertResultToMessage (ReplaceStory model.story.id model.expanded x) (ErrorMsg model.story.id)) model.story.id )

            ReplaceStory _ expanded url story ->
                ( { model | story = story, expanded = expanded, commentWidgetModels = [] }, removeFromLog url )

            HideCommentsRec _ ->
                let
                    messages =
                        List.map (\(CW.Model cm) -> CommentMsg model.story.id <| CW.HideCommentsRec (getId cm.comment) True) model.commentWidgetModels

                    newModel =
                        List.foldl
                            (\msg m ->
                                let
                                    ( newm, _ ) =
                                        update msg m
                                in
                                    newm
                            )
                            model
                            messages
                in
                    ( newModel, Cmd.none )

            LoadComments _ story mfn ->
                let
                    cmds =
                        case story.childIds of
                            Just cids ->
                                List.map (HN.getComment model.story.forum (\x -> convertResultToMessage (mfn x) (ErrorMsg model.story.id))) cids

                            Nothing ->
                                []
                in
                    ( { model | expanded = True }, Cmd.batch cmds )

            AddCommentAndLoadReplies _ url comment ->
                let
                    ( model_1, cmd_1 ) =
                        update (AddComment model.story.id url comment) model

                    ( model_2, cmd_2 ) =
                        update (CommentMsg model.story.id <| CW.LoadComments (getId comment) <| CW.AddCommentAndLoadReplies (getId comment)) model_1
                in
                    ( model_2, Cmd.batch [ removeFromLog url, cmd_1, cmd_2 ] )

            AddComment _ url comment ->
                let
                    nextIdx =
                        List.length model.commentWidgetModels

                    existingCommentIds =
                        List.map (\(CW.Model model) -> (getId model.comment)) model.commentWidgetModels

                    cms =
                        model.commentWidgetModels
                in
                    if List.member (getId comment) existingCommentIds then
                        ( model, removeFromLog url )
                    else
                        ( { model | commentWidgetModels = (CW.makeModel comment) :: cms }, removeFromLog url )
    else
        ( model, Cmd.none )


makeModel : Story -> Model
makeModel s =
    { story = s, expanded = False, commentWidgetModels = [] }


getIdx : Msg -> StoryId
getIdx msg =
    case msg of
        ErrorMsg idx _ ->
            idx

        LoadComments idx _ _ ->
            idx

        AddComment idx _ _ ->
            idx

        AddCommentAndLoadReplies idx _ _ ->
            idx

        CommentMsg idx _ ->
            idx

        HideComments idx ->
            idx

        HideCommentsRec idx ->
            idx

        Refresh idx ->
            idx

        ReplaceStory idx _ _ _ ->
            idx
