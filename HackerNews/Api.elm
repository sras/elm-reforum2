module HackerNews.Api exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Http
import Json.Decode as Decode
import Forum exposing (..)
import HackerNews.Story exposing (..)
import HackerNews.Comment exposing (..)
import Ports exposing (..)


getStories : Section -> (String -> Result Http.Error (List StoryId) -> b) -> Cmd b
getStories section msfn =
    let
        url =
            ("https://hacker-news.firebaseio.com/v0/" ++ section ++ ".json")
    in
        Http.send (msfn url) <| Http.get url <| Decode.list (Decode.map (StoryId << toString) Decode.int)


getStory : Section -> (String -> Result Http.Error Story -> b) -> StoryId -> Cmd b
getStory section msfn (StoryId id) =
    let
        makeStory a1 a2 a3 a4 a5 a6 a7 a8 =
            let
                commentCount =
                    case a6 of
                        Just x ->
                            List.length x

                        Nothing ->
                            0
            in
                Story section
                    a1
                    a2
                    ("https://news.ycombinator.com/item?id="
                        ++ (let
                                (StoryId id) =
                                    a5
                            in
                                id
                           )
                    )
                    a3
                    a4
                    a5
                    commentCount
                    a6
                    a7
                    a8

        url =
            (String.concat [ "https://hacker-news.firebaseio.com/v0/item/", id, ".json" ])

        storyDecoder : Decode.Decoder Story
        storyDecoder =
            Decode.map8 makeStory (Decode.field "title" Decode.string) (Decode.maybe <| Decode.field "url" Decode.string) (Decode.field "by" Decode.string) (Decode.maybe <| Decode.field "text" Decode.string) (Decode.map (StoryId << toString) <| Decode.field "id" Decode.int) (Decode.maybe <| Decode.field "kids" (Decode.list (Decode.map (CommentId << toString) Decode.int))) (Decode.succeed []) (Decode.field "score" Decode.int)

        request =
            Http.get url storyDecoder
    in
        Cmd.batch [ pushToLog url, Http.send (msfn url) request ]


getComment : Section -> (String -> Result Http.Error Comment -> b) -> CommentId -> Cmd b
getComment section msfn (CommentId id) =
    let
        linkToComment commentId =
            "https://news.ycombinator.com/item?id="
                ++ commentId

        deletedCommentDecoder : Decode.Decoder Comment
        deletedCommentDecoder =
            Decode.map2 (\b id -> Deleted (CommentId <| toString id)) (Decode.field "deleted" Decode.bool) (Decode.field "id" Decode.int)

        commentDecoder : Decode.Decoder Comment
        commentDecoder =
            Decode.map4
                (\id text by kids ->
                    Comment
                        { id = id
                        , section = section
                        , linkToComment =
                            let
                                (CommentId intId) =
                                    id
                            in
                                linkToComment intId
                        , text = text
                        , author = by
                        , childIds = kids
                        }
                )
                (Decode.map (CommentId << toString) <| Decode.field "id" Decode.int)
                (Decode.field "text" Decode.string)
                (Decode.field "by" Decode.string)
                (Decode.maybe <| Decode.field "kids" <| Decode.list <| (Decode.map (CommentId << toString) Decode.int))

        url =
            (String.concat [ "https://hacker-news.firebaseio.com/v0/item/", id, ".json" ])

        request =
            Http.get url <| Decode.oneOf [ commentDecoder, deletedCommentDecoder ]
    in
        Cmd.batch [ pushToLog url, Http.send (msfn url) request ]
