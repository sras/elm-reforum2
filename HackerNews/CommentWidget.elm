module HackerNews.CommentWidget exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Forum exposing (..)
import HackerNews.Api as HN
import HackerNews.Story exposing (..)
import HackerNews.Comment exposing (..)
import Common exposing (..)
import Json.Encode
import Ports exposing (..)
import Http


type Model
    = Model
        { comment : Comment
        , expanded : Bool
        , commentWidgetModels : List Model
        , parentText : Maybe String
        }


type ParentMessage
    = SendCommentText


type Msg
    = LoadComments CommentId (String -> Comment -> Msg)
    | AddComment CommentId String Comment
    | AddCommentAndLoadReplies CommentId String Comment
    | HideComments CommentId
    | ReplaceComment CommentId String Comment
    | Refresh CommentId
    | HideCommentsRec CommentId Bool
    | ErrorMsg CommentId String
    | LoadParentText CommentId String
    | MessageForParent CommentId ParentMessage
    | ChildCommentMsg CommentId Msg


view : Model -> Html Msg
view (Model model) =
    case model.comment of
        Deleted _ ->
            div [ class "comment-container" ] [ text "-- Deleted Comment --" ]

        Comment comment ->
            let
                nothingSection =
                    span []
                        [ span [ class "link", onClick <| MessageForParent (getId model.comment) SendCommentText ] [ text "Show parent" ]
                        , span [ class "link", onClick (Refresh comment.id) ] [ text "Refresh" ]
                        , a [ class "link", target "_blank", href comment.linkToComment ] [ text "Forum link" ]
                        ]

                loadMoreComments =
                    case comment.childIds of
                        Nothing ->
                            nothingSection

                        Just [] ->
                            nothingSection

                        Just cids ->
                            let
                                childCount =
                                    toString <| List.length cids
                            in
                                if not model.expanded then
                                    span []
                                        [ span [ class "link", onClick (LoadComments comment.id (AddComment comment.id)) ] [ text ("Load replies(" ++ childCount ++ ")") ]
                                        , span [ class "link", onClick (LoadComments comment.id (AddCommentAndLoadReplies comment.id)) ] [ text "Load all replies" ]
                                        , span [] [ span [ class "link", onClick (Refresh comment.id) ] [ text "Refresh" ] ]
                                        , span [ class "link", onClick <| MessageForParent (getId model.comment) SendCommentText ] [ text "Show parent" ]
                                        , a [ class "link", target "_blank", href comment.linkToComment ] [ text "Forum link" ]
                                        ]
                                else
                                    span []
                                        [ span [ class "link", onClick (HideComments comment.id) ]
                                            [ text "Collapse" ]
                                        , span [ class "link", onClick (Refresh (getId model.comment)) ] [ text "Refresh" ]
                                        , span [ class "link", onClick <| MessageForParent (getId model.comment) SendCommentText ] [ text "Show parent" ]
                                        , a [ class "link", target "_blank", href comment.linkToComment ] [ text "Forum link" ]
                                        , span [ class "link", onClick (LoadComments comment.id (AddCommentAndLoadReplies comment.id)) ] [ text ("Load replies(" ++ childCount ++ ")") ]
                                        , span
                                            [ class "link", onClick (HideCommentsRec comment.id False) ]
                                            [ text "Top Level Comments Only" ]
                                        ]
            in
                div
                    [ if not model.expanded then
                        class "container-fluid comment-container"
                      else
                        class "container-fluid comment-container expanded"
                    , id <| "comment-" ++ (toString <| getId <| model.comment)
                    ]
                    [ div [ class "row" ]
                        [ div [ class "r-border cr-border", onClick (HideComments comment.id), property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                        , div [ class "col-11", style [ ( "padding-right", "0px" ) ] ]
                            [ case model.parentText of
                                Just pt ->
                                    div [ class "parent-comment", property "innerHTML" <| Json.Encode.string <| pt ] []

                                Nothing ->
                                    span [] []
                            , div [ property "innerHTML" <| Json.Encode.string comment.text ] []
                            , span []
                                [ i [ class "author" ] [ text "- ", text comment.author ]
                                ]
                            , span [] [ loadMoreComments ]
                            , if model.expanded then
                                div [] <| List.map (\x -> Html.map (ChildCommentMsg comment.id) <| view x) model.commentWidgetModels
                              else
                                span [] []
                            ]
                        ]
                    ]


makeModel : Comment -> Model
makeModel c =
    Model { parentText = Nothing, comment = c, expanded = False, commentWidgetModels = [] }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model model) =
    case model.comment of
        Deleted _ ->
            ( Model model, Cmd.none )

        Comment comment ->
            if getIdx msg == comment.id then
                case msg of
                    LoadParentText _ t ->
                        ( Model { model | parentText = Just t }, Cmd.none )

                    MessageForParent _ _ ->
                        ( Model model, Cmd.none )

                    ErrorMsg _ _ ->
                        ( Model model, Cmd.none )

                    ReplaceComment _ url comment ->
                        ( Model { model | comment = comment, expanded = False, commentWidgetModels = [] }, removeFromLog url )

                    HideComments _ ->
                        ( Model { model | expanded = False }, scrollToElement ("comment-" ++ (toString <| getId model.comment)) )

                    Refresh _ ->
                        ( Model model, HN.getComment (getForum model.comment) (\x -> convertResultToMessage (ReplaceComment (getId model.comment) x) (ErrorMsg (getId model.comment))) <| getId model.comment )

                    HideCommentsRec _ hideSelf ->
                        let
                            messages =
                                List.filterMap
                                    (\(Model cm) ->
                                        case cm.comment of
                                            Comment c ->
                                                Just <| ChildCommentMsg comment.id <| HideCommentsRec c.id True

                                            Deleted _ ->
                                                Nothing
                                    )
                                    model.commentWidgetModels

                            ( Model newModel, cmd ) =
                                foldModel messages update (Model model)
                        in
                            ( Model { newModel | expanded = not hideSelf }, cmd )

                    ChildCommentMsg _ msg ->
                        let
                            ( models, cmd ) =
                                case msg of
                                    MessageForParent cid SendCommentText ->
                                        mapMessage (LoadParentText cid <| getText model.comment) model.commentWidgetModels update <| ChildCommentMsg <| getId model.comment

                                    _ ->
                                        mapMessage
                                            msg
                                            model.commentWidgetModels
                                            update
                                        <|
                                            ChildCommentMsg comment.id
                        in
                            ( Model { model | commentWidgetModels = models }, cmd )

                    AddCommentAndLoadReplies _ url (Deleted commentId) ->
                        update (AddComment comment.id url (Deleted commentId)) (Model model)

                    AddCommentAndLoadReplies _ url (Comment newComment) ->
                        let
                            ( model_1, cmd_1 ) =
                                update (AddComment comment.id url (Comment newComment)) (Model model)

                            ( model_2, cmd_2 ) =
                                update (ChildCommentMsg comment.id <| LoadComments newComment.id (AddCommentAndLoadReplies newComment.id)) model_1
                        in
                            ( model_2, Cmd.batch [ removeFromLog url, cmd_1, cmd_2 ] )

                    AddComment _ url (Deleted commentId) ->
                        let
                            existingCommentIds =
                                (List.map
                                    (\(Model m) -> getId m.comment)
                                    model.commentWidgetModels
                                )
                        in
                            let
                                nm =
                                    if List.member commentId existingCommentIds then
                                        Model model
                                    else
                                        Model { model | commentWidgetModels = (makeModel (Deleted commentId)) :: model.commentWidgetModels }
                            in
                                ( nm, removeFromLog url )

                    AddComment _ url (Comment newComment) ->
                        let
                            existingCommentIds =
                                (List.map
                                    (\(Model m) -> getId m.comment)
                                    model.commentWidgetModels
                                )

                            nm =
                                if List.member newComment.id existingCommentIds then
                                    model
                                else
                                    { model | commentWidgetModels = (makeModel (Comment newComment)) :: model.commentWidgetModels }
                        in
                            ( Model nm, removeFromLog url )

                    LoadComments commentId addMsg ->
                        let
                            cmds =
                                case comment.childIds of
                                    Just cids ->
                                        List.map (HN.getComment (getForum model.comment) (\x -> convertResultToMessage (addMsg x) (ErrorMsg commentId))) cids

                                    Nothing ->
                                        []
                        in
                            ( Model
                                { model
                                    | expanded =
                                        case comment.childIds of
                                            Just cids ->
                                                if List.length cids > 0 then
                                                    True
                                                else
                                                    False

                                            Nothing ->
                                                False
                                }
                            , Cmd.batch cmds
                            )
            else
                ( Model model, Cmd.none )


getIdx : Msg -> CommentId
getIdx msg =
    case msg of
        LoadComments idx _ ->
            idx

        ChildCommentMsg idx _ ->
            idx

        ErrorMsg idx _ ->
            idx

        AddComment idx _ _ ->
            idx

        AddCommentAndLoadReplies idx _ _ ->
            idx

        HideComments idx ->
            idx

        Refresh idx ->
            idx

        HideCommentsRec idx _ ->
            idx

        ReplaceComment idx _ _ ->
            idx

        LoadParentText idx _ ->
            idx

        MessageForParent idx _ ->
            idx
