module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import ForumWidget as FW
import Ports exposing (..)
import Common exposing (..)
import Watches as W
import Filters as F
import Time
import Forum exposing (..)
import HackerNews.Widget as HN
import Reddit.Widget as RD
import Json.Decode as Decode


type Route
    = Index
    | Filters
    | Watches


type alias Model =
    { forumsModel : FW.Model
    , logs : List String
    , route : Route
    , watchesModel : W.Model
    , filtersModel : F.Model
    }


type Msg
    = ForumWidgetMsg FW.Msg
    | LogMessage String
    | DeLogMessage String
    | ClearLog
    | ChangeRoute Route
    | WatchesMsg W.Msg
    | FiltersMsg F.Msg
    | WatchedItemFound String
    | Tick Time.Time
    | LoadConfig String


makeSettings : Model -> Settings
makeSettings model =
    { watches = model.watchesModel.watches
    , filters = model.filtersModel.filters
    , forums = FW.getForumList model.forumsModel.forumModels
    }


affectSettings : Settings -> Model -> ( Model, Cmd Msg )
affectSettings settings model =
    let
        addForumMsgs : List Msg
        addForumMsgs =
            List.map (\x -> ForumWidgetMsg <| FW.AddForum x) settings.forums

        addWatchMsgs : List Msg
        addWatchMsgs =
            List.map (\x -> WatchesMsg <| W.AddWatch x) settings.watches

        addFilterMsgs : List Msg
        addFilterMsgs =
            List.map (\x -> FiltersMsg <| F.AddFilter x) settings.filters
    in
        foldModel (List.concat [ addForumMsgs, addWatchMsgs, addFilterMsgs ]) update model


view : Model -> Html Msg
view model =
    let
        currentView =
            case model.route of
                Index ->
                    div [] [ Html.map ForumWidgetMsg <| FW.view model.forumsModel ]

                Filters ->
                    div [] [ Html.map FiltersMsg (F.view model.filtersModel) ]

                Watches ->
                    div [] [ Html.map WatchesMsg (W.view model.watchesModel) ]
    in
        div [ class "container-fluid" ]
            [ div [ class "row" ]
                [ div [ class "col-lg-12" ]
                    [ div []
                        [--span [ onClick <| ChangeRoute Index, class "link" ] [ text "Forums" ]
                         -- , span [ onClick <| ChangeRoute Watches, class "link" ] [ text "Watches" ]
                         -- , span [ onClick <| ChangeRoute Filters, class "link" ] [ text "Filters" ]
                        ]
                    ]
                ]
            , div [ class "col-lg-12" ] [ currentView ]
            , if List.length model.logs > 0 then
                div [ class "log-container" ] <| [ div [] [ div [ id "log-container", class "log-items-container" ] <| List.map (\x -> div [ class "log-item" ] [ text x ]) model.logs, div [ onClick ClearLog, class "clear-link" ] [ text "clear" ] ] ]
              else
                span [] []
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeRoute r ->
            ( { model | route = r }, Cmd.none )

        ForumWidgetMsg m ->
            let
                ( fm, c ) =
                    FW.update (makeSettings model) m model.forumsModel

                ( newModel, newCommand ) =
                    ( { model | forumsModel = fm }, Cmd.map ForumWidgetMsg c )
            in
                case m of
                    FW.AddNewForum ->
                        ( newModel, Cmd.batch [ newCommand, saveSettings <| encodeSettings <| makeSettings newModel ] )

                    FW.RedditMsg (RD.Remove _) ->
                        ( newModel, Cmd.batch [ newCommand, saveSettings <| encodeSettings <| makeSettings newModel ] )

                    FW.HackerNewsMsg (HN.Remove _) ->
                        ( newModel, Cmd.batch [ newCommand, saveSettings <| encodeSettings <| makeSettings newModel ] )

                    _ ->
                        ( newModel, newCommand )

        LogMessage message ->
            let
                l =
                    model.logs
            in
                ( { model | logs = message :: l }, Cmd.none )

        ClearLog ->
            ( { model | logs = [] }, Cmd.none )

        WatchesMsg msg ->
            let
                m =
                    { model | watchesModel = W.update msg model.watchesModel }
            in
                ( m, saveSettings <| encodeSettings <| makeSettings m )

        FiltersMsg msg ->
            let
                m =
                    { model | filtersModel = F.update msg model.filtersModel }
            in
                ( m, saveSettings <| encodeSettings <| makeSettings m )

        WatchedItemFound title ->
            update (WatchesMsg <| W.AddResult title) model

        Tick time ->
            update (ForumWidgetMsg FW.Load) model

        LoadConfig c ->
            case Decode.decodeString settingsDecoder c of
                Ok settings ->
                    affectSettings settings model

                Err s ->
                    log s <| ( model, Cmd.none )

        DeLogMessage message ->
            let
                l =
                    model.logs
            in
                ( { model | logs = removeItemFromList l message }, Cmd.none )


init : ( Model, Cmd Msg )
init =
    let
        startingModel =
            { forumsModel = FW.init, filtersModel = F.init, logs = [], route = Index, watchesModel = W.init }

        ( model, cmd ) =
            foldModel [] update startingModel
    in
        ( model, cmd )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ loadConfig LoadConfig, logItem LogMessage, deLogItem DeLogMessage, watchedItemFound WatchedItemFound ]


main : Program Never Model Msg
main =
    program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
