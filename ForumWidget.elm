module ForumWidget exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Forum exposing (..)
import HackerNews.Widget as HN
import Reddit.Widget as RD
import Yify.Widget as YF
import Common exposing (..)
import Ports exposing (..)
import Http


type ForumModel
    = RedditForumModel RD.Model
    | HackerNewsForumModel HN.Model
    | YifyModel YF.Model


type alias Model =
    { forumModels : List ForumModel, newForumName : String, newForumSection : String }


type Msg
    = RedditMsg RD.Msg
    | HackerNewsMsg HN.Msg
    | YifyMsg YF.Msg
    | AddForum Forum
    | ForumNameInput String
    | SectionNameInput String
    | AddNewForum
    | Load


getForumList : List ForumModel -> List Forum
getForumList fms =
    let
        fmToforum : ForumModel -> Forum
        fmToforum fm =
            case fm of
                RedditForumModel m ->
                    Reddit m.subreddit

                HackerNewsForumModel m ->
                    HackerNews m.section

                YifyModel _ ->
                    Yify
    in
        List.map fmToforum fms


getIdx : ForumModel -> Int
getIdx fm =
    case fm of
        RedditForumModel m ->
            m.idx

        HackerNewsForumModel m ->
            m.idx

        YifyModel m ->
            m.idx


init : Model
init =
    let
        m =
            { forumModels = [], newForumName = "", newForumSection = "" }

        msgs =
            []

        settings =
            { watches = [], filters = [], forums = [] }

        applyMsg x m =
            let
                ( newModel, _ ) =
                    update settings x m
            in
                newModel
    in
        List.foldl applyMsg m msgs


loadForum : Settings -> ForumModel -> ( ForumModel, Cmd Msg )
loadForum watches fm =
    case fm of
        YifyModel md ->
            let
                ( m, c ) =
                    YF.update watches (YF.Load md.idx) md
            in
                ( YifyModel m, Cmd.map YifyMsg c )

        RedditForumModel md ->
            let
                ( m, c ) =
                    RD.update watches (RD.Load md.idx) md
            in
                ( RedditForumModel m, Cmd.map RedditMsg c )

        HackerNewsForumModel md ->
            let
                ( m, c ) =
                    HN.update watches (HN.Load md.idx) md
            in
                ( HackerNewsForumModel m, Cmd.map HackerNewsMsg c )


renderForum : Model -> ForumModel -> Html Msg
renderForum model fm =
    let
        forumWidgetView =
            case fm of
                HackerNewsForumModel m ->
                    Html.map HackerNewsMsg <| HN.view m

                RedditForumModel m ->
                    Html.map RedditMsg <| RD.view m

                YifyModel m ->
                    Html.map YifyMsg <| YF.view m
    in
        forumWidgetView


applyYifyMsg : Settings -> YF.Msg -> ForumModel -> ( ForumModel, Cmd YF.Msg )
applyYifyMsg settings ymsg fm =
    case fm of
        YifyModel m ->
            let
                ( nm, c ) =
                    YF.update settings ymsg m
            in
                ( YifyModel nm, c )

        m ->
            ( m, Cmd.none )


applyRedditMsg : Settings -> RD.Msg -> ForumModel -> ( ForumModel, Cmd RD.Msg )
applyRedditMsg settings rmsg fm =
    case fm of
        RedditForumModel m ->
            let
                ( nm, c ) =
                    RD.update settings rmsg m
            in
                ( RedditForumModel nm, c )

        m ->
            ( m, Cmd.none )


applyHackerNewsMsg : Settings -> HN.Msg -> ForumModel -> ( ForumModel, Cmd HN.Msg )
applyHackerNewsMsg settings hmsg fm =
    case fm of
        HackerNewsForumModel m ->
            let
                ( nm, c ) =
                    HN.update settings hmsg m
            in
                ( HackerNewsForumModel nm, c )

        m ->
            ( m, Cmd.none )


view : Model -> Html Msg
view model =
    let
        sampleInput =
            if model.newForumName == "HackerNews" then
                "HN sections such as topstories, askstories etc"
            else if model.newForumName == "Reddit" then
                "Subreddit name like askreddit, programming, movies etc"
            else
                ""
    in
        div [ class "container-fluid" ]
            [ div [ class "row" ]
                [ div [ class "col-lg-12" ]
                    [ div [ class "container-fluid" ]
                        [ div [ class "row" ]
                            [ div [ class "col-md-8 new-forum-form" ]
                                [ select [ onInput ForumNameInput ]
                                    [ option [ value "" ]
                                        [ text "-- Select forum --" ]
                                    , option [ value "Reddit" ]
                                        [ text "Reddit" ]
                                    , option
                                        [ value "HackerNews" ]
                                        [ text "HackerNews" ]
                                    , option
                                        [ value "Yify" ]
                                        [ text "Yify" ]
                                    ]
                                , if model.newForumName == "Yify" then
                                    span [] []
                                  else
                                    input [ placeholder sampleInput, onInput SectionNameInput, type_ "text", value model.newForumSection ] []
                                , button [ onClick AddNewForum ] [ text "Add" ]
                                ]
                            , div [ class "col-md-2 d-none d-sm-block" ] []
                            ]
                        ]
                    ]
                ]
            , div [ class "row" ]
                [ div [ class "col-12" ] <| List.map (renderForum model) model.forumModels ]
            ]


notYify : ForumModel -> Bool
notYify fm =
    case fm of
        RedditForumModel _ ->
            True

        HackerNewsForumModel _ ->
            True

        YifyModel _ ->
            False


update : Settings -> Msg -> Model -> ( Model, Cmd Msg )
update settings msg model =
    case msg of
        Load ->
            let
                updates =
                    List.map
                        (\x ->
                            if notYify x then
                                loadForum settings x
                            else
                                ( x, Cmd.none )
                        )
                        model.forumModels
            in
                ( { model | forumModels = List.map Tuple.first updates }, Cmd.batch <| List.map Tuple.second updates )

        ForumNameInput s ->
            ( { model | newForumName = s }, Cmd.none )

        SectionNameInput s ->
            ( { model | newForumSection = s }, Cmd.none )

        AddNewForum ->
            if model.newForumName == "Reddit" then
                update settings (AddForum <| Reddit model.newForumSection) model
            else if model.newForumName == "HackerNews" then
                update settings (AddForum <| HackerNews model.newForumSection) model
            else if model.newForumName == "Yify" then
                update settings (AddForum Yify) model
            else
                ( model, Cmd.none )

        AddForum forum ->
            case forum of
                Yify ->
                    let
                        ( yfmodel, cmd ) =
                            YF.init (List.length model.forumModels)
                    in
                        ( { model | forumModels = YifyModel yfmodel :: model.forumModels }, Cmd.map YifyMsg cmd )

                Reddit subname ->
                    let
                        ( rdmodel, cmd ) =
                            RD.init subname (List.length model.forumModels)
                    in
                        ( { model | forumModels = RedditForumModel rdmodel :: model.forumModels }, Cmd.map RedditMsg cmd )

                HackerNews section ->
                    let
                        ( hnmodel, cmd ) =
                            HN.init section (List.length model.forumModels)
                    in
                        ( { model | forumModels = HackerNewsForumModel hnmodel :: model.forumModels }, Cmd.map HackerNewsMsg cmd )

        RedditMsg rmsg ->
            case rmsg of
                RD.Remove idx ->
                    ( { model | forumModels = List.filter (\x -> not <| (getIdx x) == idx) model.forumModels }, Cmd.none )

                _ ->
                    let
                        ( models, cmd ) =
                            mapMessage rmsg
                                model.forumModels
                                (applyRedditMsg
                                    settings
                                )
                                RedditMsg
                    in
                        ( { model | forumModels = models }, cmd )

        YifyMsg ymsg ->
            case ymsg of
                YF.Remove idx ->
                    ( { model | forumModels = List.filter (\x -> not <| (getIdx x) == idx) model.forumModels }, Cmd.none )

                _ ->
                    let
                        ( models, cmd ) =
                            mapMessage ymsg
                                model.forumModels
                                (applyYifyMsg
                                    settings
                                )
                                YifyMsg
                    in
                        ( { model | forumModels = models }, cmd )

        HackerNewsMsg rmsg ->
            case rmsg of
                HN.Remove idx ->
                    ( { model | forumModels = List.filter (\x -> not <| (getIdx x) == idx) model.forumModels }, Cmd.none )

                _ ->
                    let
                        ( models, cmd ) =
                            mapMessage rmsg model.forumModels (applyHackerNewsMsg settings) HackerNewsMsg
                    in
                        ( { model | forumModels = models }, cmd )
