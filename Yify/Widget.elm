module Yify.Widget exposing (..)

import Debug exposing (log, crash)
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Debug exposing (log, crash)
import Yify.Api as Api
import Common exposing (..)
import Forum exposing (..)
import Ports exposing (..)
import Json.Encode
import Random


defaultParams : Api.Params
defaultParams =
    { term = Nothing, minRating = Nothing, genre = Nothing, sortBy = Nothing, orderBy = Nothing, page = Nothing }


type alias Model =
    { idx : Int, expanded : Bool, params : Api.Params, result : Maybe Api.SearchResult }


type Param
    = Genre
    | Term
    | MinRating
    | SortBy
    | OrderBy


type Msg
    = Expand Int
    | Collapse Int
    | Remove Int
    | Load Int
    | AddMovies Int Url (Maybe Api.SearchResult) Api.SearchResult
    | ErrorMsg Int String
    | ParamInput Int Param String
    | LoadMore Int
    | Randomize Int Api.SearchResult
    | LoadPageUsingParams Int Api.Params Int


paramView : Model -> Html Msg
paramView model =
    div []
        [ div []
            [ input [ size 5, onInput <| ParamInput model.idx Term, placeholder "search term" ] []
            , select
                [ onInput <| ParamInput model.idx Genre ]
                [ option [ value "" ] [ text "-- Select genre --" ]
                , option [ value "Action" ] [ text "Action" ]
                , option [ value "Adventure" ] [ text "Adventure" ]
                , option [ value "Animation" ] [ text "Animation" ]
                , option [ value "Biography" ] [ text "Biography" ]
                , option [ value "Comedy" ] [ text "Comedy" ]
                , option [ value "Crime" ] [ text "Crime" ]
                , option [ value "Documentary" ] [ text "Documentary" ]
                , option [ value "Drama" ] [ text "Drama" ]
                , option [ value "Family" ] [ text "Family" ]
                , option [ value "Fantasy" ] [ text "Fantasy" ]
                , option [ value "Film Noir" ] [ text "Film Noir" ]
                , option [ value "History" ] [ text "History" ]
                , option [ value "Horror" ] [ text "Horror" ]
                , option [ value "Music" ] [ text "Music" ]
                , option [ value "Musical" ] [ text "Musical" ]
                , option [ value "Mystery" ] [ text "Mystery" ]
                , option [ value "Romance" ] [ text "Romance" ]
                , option [ value "Sci-Fi" ] [ text "Sci-Fi" ]
                , option [ value "Short" ] [ text "Short" ]
                , option [ value "Sport" ] [ text "Sport" ]
                , option [ value "Superhero" ] [ text "Superhero" ]
                , option [ value "Thriller" ] [ text "Thriller" ]
                , option [ value "War" ] [ text "War" ]
                , option [ value "Western" ] [ text "Western" ]
                ]
            , select [ onInput <| ParamInput model.idx MinRating ]
                [ option [ value "" ] [ text "-- Select rating --" ]
                , option [ value "1" ] [ text "1" ]
                , option [ value "2" ] [ text "2" ]
                , option [ value "3" ] [ text "3" ]
                , option [ value "4" ] [ text "4" ]
                , option [ value "5" ] [ text "5" ]
                , option [ value "6" ] [ text "6" ]
                , option [ value "7" ] [ text "7" ]
                , option [ value "8" ] [ text "8" ]
                , option [ value "9" ] [ text "9" ]
                ]
            , select [ onInput <| ParamInput model.idx SortBy ]
                [ option [ value "" ] [ text "-- Select sort param --" ]
                , option [ value "title" ] [ text "title" ]
                , option [ value "year" ] [ text "year" ]
                , option [ value "rating" ] [ text "rating" ]
                , option [ value "date_added" ] [ text "added date" ]
                ]
            , select [ onInput <| ParamInput model.idx OrderBy ]
                [ option [ value "" ] [ text "-- Select sort order --" ]
                , option [ value "asc" ] [ text "Ascending" ]
                , option [ value "desc" ] [ text "Descending" ]
                ]
            , button [ onClick <| Load model.idx ] [ text "Refresh" ]
            , case model.result of
                Nothing ->
                    span [] []

                Just result ->
                    if result.total > 20 then
                        button [ onClick <| Randomize model.idx result ] [ text "Randomize" ]
                    else
                        span [] []
            ]
        ]


torrentView : Api.Torrent -> Html Msg
torrentView torrent =
    a [ class "link", href torrent.url ] [ text torrent.quality ]


movieView : Api.Movie -> Html Msg
movieView movie =
    div [ class "movie-container" ]
        [ div [ class "movie-image-synopsis" ]
            [ img [ style [ ( "flex-shrink", "0" ) ], src movie.imageUrl ] []
            , div []
                [ text movie.name
                , i [] [ text <| "(" ++ toString movie.year ++ ")" ]
                , div
                    [ class "movie-info" ]
                    [ small [] [ text <| (String.join "," movie.genres) ]
                    , small [] [ text <| (toString movie.rating) ]
                    , let
                        (Api.ImdbId i) =
                            movie.imdbCode
                      in
                        a [ target "_blank", href ("https://www.imdb.com/title/" ++ i ++ "/reviews") ] [ small [] [ text "imdb" ] ]
                    ]
                , div [ class "movie-plot" ] [ text movie.description ]
                , div [] <| List.map torrentView movie.torrents
                ]
            ]
        ]


view : Model -> Html Msg
view model =
    div [ class "forum-container" ]
        [ div
            [ class "container-fluid" ]
            [ div [ class "row" ]
                [ div [ class "fr-border r-border", onClick <| Collapse model.idx, property "innerHTML" (Json.Encode.string "&nbsp;") ] []
                , div [ class "col-11" ]
                    [ span
                        [ onClick <|
                            if model.expanded then
                                Collapse model.idx
                            else
                                Expand model.idx
                        , class "forum-heading"
                        ]
                        [ text "YIFY" ]
                    , span [ class "link", onClick <| Remove model.idx ] [ text "remove" ]
                    , if model.expanded then
                        div []
                            [ paramView model
                            , div [] <|
                                case model.result of
                                    Just sr ->
                                        List.map movieView sr.movies

                                    Nothing ->
                                        []
                            ]
                      else
                        span [] []
                    , if model.expanded then
                        div [ class "link", style [ ( "fontSize", "20px" ) ] ]
                            [ span
                                [ onClick <| LoadMore model.idx ]
                                [ text "Load more" ]
                            ]
                      else
                        div [] []
                    ]
                ]
            ]
        ]


update : Settings -> Msg -> Model -> ( Model, Cmd Msg )
update settings msg model =
    if getIdx msg == model.idx then
        case msg of
            Expand _ ->
                if model.result == Nothing then
                    update settings (Load model.idx) { model | expanded = True }
                else
                    ( { model | expanded = True }, Cmd.none )

            Collapse _ ->
                ( { model | expanded = False }, Cmd.none )

            Remove _ ->
                ( model, Cmd.none )

            AddMovies _ url prevResult searchResult ->
                case prevResult of
                    Just pr ->
                        ( { model | result = Just { searchResult | movies = pr.movies ++ searchResult.movies } }, removeFromLog url )

                    Nothing ->
                        ( { model | result = Just searchResult }, removeFromLog url )

            ParamInput _ paramtype inp ->
                let
                    p =
                        model.params

                    emptyToNothing : String -> Maybe String
                    emptyToNothing s =
                        if s == "" then
                            Nothing
                        else
                            Just s
                in
                    case paramtype of
                        Genre ->
                            ( { model | params = { p | genre = emptyToNothing inp } }, Cmd.none )

                        Term ->
                            ( { model | params = { p | term = emptyToNothing inp } }, Cmd.none )

                        MinRating ->
                            ( { model | params = { p | minRating = emptyToNothing inp } }, Cmd.none )

                        SortBy ->
                            ( { model | params = { p | sortBy = emptyToNothing inp } }, Cmd.none )

                        OrderBy ->
                            ( { model | params = { p | orderBy = emptyToNothing inp } }, Cmd.none )

            LoadPageUsingParams _ params page ->
                let
                    cmd =
                        Api.getMovies { params | page = Just page } (\x -> convertResultToMessage (AddMovies model.idx x Nothing) (ErrorMsg model.idx))
                in
                    ( model, cmd )

            LoadMore _ ->
                let
                    cmd =
                        case model.result of
                            Just result ->
                                let
                                    params =
                                        result.params
                                in
                                    Api.getMovies { params | page = Just <| result.page + 1 } (\x -> convertResultToMessage (AddMovies model.idx x model.result) (ErrorMsg model.idx))

                            Nothing ->
                                Cmd.none
                in
                    ( model, cmd )

            Load _ ->
                let
                    cmd =
                        Api.getMovies model.params (\x -> convertResultToMessage (AddMovies model.idx x Nothing) (ErrorMsg model.idx))
                in
                    ( model, cmd )

            Randomize _ result ->
                let
                    pages =
                        Api.totalPages result

                    generator =
                        Random.int 1 pages
                in
                    ( model, Random.generate (LoadPageUsingParams model.idx result.params) generator )

            ErrorMsg _ err ->
                log err <| ( model, Cmd.none )
    else
        ( model, Cmd.none )


init : Int -> ( Model, Cmd Msg )
init idx =
    ( { result = Nothing, idx = idx, expanded = False, params = defaultParams }, Cmd.none )


getIdx : Msg -> Int
getIdx msg =
    case msg of
        Expand idx ->
            idx

        Collapse idx ->
            idx

        Remove idx ->
            idx

        Load idx ->
            idx

        AddMovies idx _ _ _ ->
            idx

        ErrorMsg idx _ ->
            idx

        ParamInput idx _ _ ->
            idx

        LoadMore idx ->
            idx

        Randomize idx _ ->
            idx

        LoadPageUsingParams idx _ _ ->
            idx
