module Yify.Api exposing (..)

import Forum exposing (..)
import Json.Decode as Decode
import Http
import Ports exposing (..)


type MovieId
    = MovieId String


type ImdbId
    = ImdbId String


type alias Params =
    { term : Maybe String, minRating : Maybe String, genre : Maybe String, sortBy : Maybe String, orderBy : Maybe String, page : Maybe Int }


type alias SearchResult =
    { params : Params, movies : List Movie, page : Int, total : Int, limit : Int }


totalPages : SearchResult -> Int
totalPages sr =
    ceiling ((toFloat sr.total) / (toFloat sr.limit))


movieIdDecoder : Decode.Decoder MovieId
movieIdDecoder =
    Decode.map MovieId Decode.string


imdbIdDecoder : Decode.Decoder ImdbId
imdbIdDecoder =
    Decode.map ImdbId Decode.string


type alias Torrent =
    { url : Url, seeds : Int, peers : Int, size : String, quality : String }


torrentDecoder : Decode.Decoder Torrent
torrentDecoder =
    Decode.map5 Torrent
        (Decode.field "url" Decode.string)
        (Decode.field "seeds" Decode.int)
        (Decode.field "peers" Decode.int)
        (Decode.field "size" Decode.string)
        (Decode.field "quality" Decode.string)


type alias Movie =
    { id : MovieId, imdbCode : ImdbId, name : String, year : Int, rating : Float, genres : List String, description : String, imageUrl : Url, torrents : List Torrent }


movieDecoder : Decode.Decoder Movie
movieDecoder =
    Decode.map2 (\t f -> f t)
        (Decode.field "torrents" <| Decode.list torrentDecoder)
    <|
        Decode.map8 Movie
            (Decode.map (MovieId << toString) <| Decode.field "id" Decode.int)
            (Decode.field "imdb_code" <| imdbIdDecoder)
            (Decode.field "title" <| Decode.string)
            (Decode.field "year" <| Decode.int)
            (Decode.field "rating" <| Decode.float)
            (Decode.field "genres" <| Decode.list Decode.string)
            (Decode.field "description_full" Decode.string)
            (Decode.field "medium_cover_image" Decode.string)


listingResultDecoder : Params -> Decode.Decoder SearchResult
listingResultDecoder params =
    Decode.field "data" <|
        Decode.map4 (SearchResult params)
            (Decode.field "movies" <| Decode.list movieDecoder)
            (Decode.field "page_number" Decode.int)
            (Decode.field "movie_count" Decode.int)
            (Decode.field "limit" Decode.int)


emptyListingResultDecoder : Params -> Decode.Decoder SearchResult
emptyListingResultDecoder params =
    Decode.succeed
        { params = params, movies = [], page = 1, total = 0, limit = 20 }


getMovies : Params -> (String -> Result Http.Error SearchResult -> b) -> Cmd b
getMovies params msfn =
    let
        url =
            let
                term =
                    case params.term of
                        Just g ->
                            "query_term=" ++ (Http.encodeUri g)

                        Nothing ->
                            ""

                genre =
                    case params.genre of
                        Just g ->
                            "genre=" ++ g

                        Nothing ->
                            ""

                rating =
                    case params.minRating of
                        Just g ->
                            "minimum_rating=" ++ g

                        Nothing ->
                            ""

                sort_by =
                    case params.sortBy of
                        Just g ->
                            "sort_by=" ++ g

                        Nothing ->
                            ""

                order_by =
                    case params.orderBy of
                        Just g ->
                            "order_by=" ++ g

                        Nothing ->
                            ""

                page =
                    case params.page of
                        Just g ->
                            "page=" ++ (toString g)

                        Nothing ->
                            ""

                queries =
                    String.join "&" <| List.filter (\x -> x /= "") [ genre, term, rating, sort_by, order_by, page ]
            in
                if String.length queries > 0 then
                    "https://yts.am/api/v2/list_movies.json?" ++ queries
                else
                    "https://yts.am/api/v2/list_movies.json"
    in
        Cmd.batch [ pushToLog url, Http.send (msfn url) <| Http.get url <| (Decode.oneOf [ listingResultDecoder params, emptyListingResultDecoder params ]) ]
