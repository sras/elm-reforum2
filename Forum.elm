module Forum exposing (..)

import Debug exposing (log, crash)
import Json.Encode exposing (..)
import Json.Decode as Decode


type alias Section =
    String


type alias Subreddit =
    String


type Forum
    = HackerNews String
    | Reddit String
    | Yify


type alias Url =
    String


type alias Settings =
    { watches : List Watch
    , filters : List String
    , forums : List Forum
    }


type alias Watch =
    { forum : Forum
    , term : String
    }


forumToString : Forum -> String
forumToString f =
    case f of
        Reddit s ->
            "/r/" ++ s

        HackerNews s ->
            "Hackernews: " ++ s

        Yify ->
            "Yify"


watchToString : Watch -> String
watchToString w =
    w.term ++ " in " ++ (forumToString w.forum)


watchDecoder : Decode.Decoder Watch
watchDecoder =
    Decode.map2 Watch (Decode.field "forum" forumDecoder) (Decode.field "term" Decode.string)


forumDecoder : Decode.Decoder Forum
forumDecoder =
    let
        hnDecoder =
            Decode.map HackerNews (Decode.field "section" Decode.string)

        redditDecoder =
            Decode.map Reddit (Decode.field "section" Decode.string)

        decoder : String -> Decode.Decoder Forum
        decoder fname =
            case fname of
                "hackernews" ->
                    hnDecoder

                "reddit" ->
                    redditDecoder

                "yify" ->
                    Decode.succeed Yify

                other ->
                    Decode.fail <| "Unknown forum " ++ other
    in
        Decode.field "forum" Decode.string |> (Decode.andThen decoder)


settingsDecoder : Decode.Decoder Settings
settingsDecoder =
    Decode.map3 Settings
        (Decode.field "watches" <| Decode.list watchDecoder)
        (Decode.field "filters" <| Decode.list Decode.string)
        (Decode.field "forums" <| Decode.list forumDecoder)


encodeForum : Forum -> Value
encodeForum f =
    case f of
        Yify ->
            object [ ( "forum", string "yify" ), ( "section", string "" ) ]

        Reddit s ->
            object [ ( "forum", string "reddit" ), ( "section", string s ) ]

        HackerNews s ->
            object [ ( "forum", string "hackernews" ), ( "section", string s ) ]


encodeWatch : Watch -> Value
encodeWatch w =
    object [ ( "forum", encodeForum w.forum ), ( "term", string w.term ) ]


encodeSettings : Settings -> String
encodeSettings settings =
    encode 0 <|
        object
            [ ( "watches", list <| List.map encodeWatch settings.watches )
            , ( "filters", list <| List.map string settings.filters )
            , ( "forums", list <| List.map encodeForum settings.forums )
            ]
