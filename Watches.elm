module Watches exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Debug exposing (log, crash)
import String exposing (toInt)
import Random exposing (..)
import Forum exposing (..)
import HackerNews.Widget as HN
import Reddit.Widget as RD
import Common exposing (..)
import Ports exposing (..)
import Http


type Msg
    = Add
    | Remove Watch
    | WatchStringInput String
    | AddWatch Watch
    | AddResult String
    | ForumNameInput String
    | SectionNameInput String


type alias Model =
    { watches : List Watch, newWatch : String, newForumSection : String, newForumName : String, results : List String }


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ div []
            [ div []
                [ input [ placeholder "Enter word to watch for", onInput WatchStringInput, value model.newWatch ] []
                , text " in "
                , select [ onInput ForumNameInput ]
                    [ option [ value "" ]
                        [ text "-- Select forum --" ]
                    , option [ value "Reddit" ]
                        [ text "Reddit" ]
                    , option
                        [ value "HackerNews" ]
                        [ text "HackerNews" ]
                    ]
                , input [ placeholder "Forum section name", onInput SectionNameInput, type_ "text", value model.newForumSection ] []
                ]
            , button [ onClick Add ] [ text "Add" ]
            ]
        , if List.length model.watches > 0 then
            div []
                [ h3 []
                    [ text "Watching for..."
                    ]
                ]
          else
            div [] [ text "No watches yet.." ]
        , div [] <| List.map (\x -> p [] [ text <| watchToString x, span [ class "link", onClick <| Remove x ] [ text "remove" ] ]) model.watches
        , if List.length model.results > 0 then
            div [] [ h4 [] [ text "Results" ] ]
          else
            div [] []
        , div [] <| List.map (\x -> p [] [ text x ]) model.results
        ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        Add ->
            if model.newForumName == "Reddit" then
                let
                    newWatch =
                        Watch (Reddit model.newForumSection) (String.toLower model.newWatch)
                in
                    { model | watches = newWatch :: model.watches, newWatch = "" }
            else if model.newForumName == "HackerNews" then
                let
                    newWatch =
                        Watch (HackerNews model.newForumSection) (String.toLower model.newWatch)
                in
                    { model | watches = newWatch :: model.watches, newWatch = "" }
            else
                model

        AddWatch w ->
            { model | watches = w :: model.watches }

        SectionNameInput s ->
            { model | newForumSection = s }

        ForumNameInput s ->
            { model | newForumName = s }

        WatchStringInput s ->
            { model | newWatch = s }

        Remove w ->
            { model | watches = List.filter (\x -> not <| x == w) model.watches }

        AddResult s ->
            { model
                | results =
                    if List.member s model.results then
                        model.results
                    else
                        s :: model.results
            }


init : Model
init =
    { watches = [], newWatch = "", newForumName = "", newForumSection = "", results = [] }
